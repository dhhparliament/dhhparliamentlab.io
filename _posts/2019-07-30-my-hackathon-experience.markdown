---
layout: post
title:  "My Hackathon experience"
date:   2019-07-29 20:00:00 -0300
categories: Update
image: dhh19-logo.png
---

The DHH19 started a lot different than I expected, from previous experience
with Hackathon’s I expected a more competitive style with a clear topic and
less freedom. But I really liked the concept of the Hackathon. My Group had the
brought Topic of the Many voices of European Parliament.  For which we got a
Dataset build from speeches in the European Parliament in the English language
. In the course of the hackathon we had to define a research question and work
on it to generate a poster and an presentation about our results. We started
this process by looking at the data we got and at the information about the
source we had. Therefor  I started to look at the data Files we had an drew a
representation of the liked files to display what information we had. Following
the basic research had a brainstorming session on what the research question
can look like and joined them into three fields of questions. Then we
subdivides our group into three and created from these fields a defined
research question. In this process my group specified a question about
relations in the European Parliament. By looking at the citations. We were
interested to see how different Parties Ages and Genders cite each other more
often. And which Member gets named in connections with a special topic. In the
following discussion we decided to follow another research question since this
one did not really use all the skills in the group but was more direct an
technical.

The research question we compromised on is the concept on Future and Past in
the European Parliament and how it changes regarding topics age country or
gender of the speaker. To analyze this we needed to define a list of words that
represent sentences or speeches which are about the past or the future. 

After defining the research question we had a presentation about the research
question to the other groups, which was quite helpful. Since we got useful
feedback.

After a refreshing weekend we started on Monday with new energy into answering
our research question.

To do so we brainstormed, used topic modeling, collocation, word embedding as
well as close reading. In addition to this we chose to look into two case
studies of Future and Past.  The case study for the Past is Totalitarianism and
the case for the Future is Climate change.  While the others started to work on
the preparation of the different Models, generated different sub corpora for
all the different Topics  we choose. For each of them I generated a dataset
with all the speeches that contained one of the keywords/ word embeddings we
selected before. I then added Information about the speaker to this data. Then
I generated a dataset which did not contain the whole speech but just the
sentence which contained the key. As well as the key which it contains.
Afterwards I added more information and changed the dataset on feedback from my team members.
For example in the list of speeches I had a speech multipliable times when it
contains multipliable keys, which distorted multiple methods.

After a while we found out that there were some speeches in the dataset, which
were not in English.  To clean the dataset from those speeches, I used a
language detection library and ran over the dataset to find all speeches not in
English. I had some problems doing that, since some speeches were very short
and contained none English names and where therefor mislabeled. With some
tweaking I removed all the none English speeches from the corpus and
regenerated all the sub corpora and required datasets for my team members.
Around this time we started to build our presentation and finish our Poster. It
was very cool to see how all the different methods and views got joined into
our finished project. In the final presentation I every team member talked
about their part in the group so I talked about the data and how we cleaned and
prepared it.

I learned a lot in the DHH19 Hackathon, for once I learned how to approach a
Humanities problem. Since I am a Computer Scientist I learned a very
engineering approach to answering questions.  Without my teammates I wouldn’t
have thought about this research question, since I did not knew how to
technically get an answer without interpretation of results which needed a lot
of domain knowledge. 
I really liked the broad field of backgrounds my Team had, this way we had a so
many views.  In the future I want to learn to use more of the different Data
science Methods.
