---
layout: post
title:  "Preprocessing the Data"
date:   2019-07-29 00:00:00 -0300
categories: jekyll update
image: europarl.png
---
The Data and how it was cleaned

For our work at the Hackathon we got presented with a Dataset containing 
247,955 speeches. For most of the speeches we got the following Meta-data.

<img style="max-width:100%;" src="{{ site.github.url }}/assets/img/data.jpg">

As you can see in the Graph, our Dataset consisted of five Tables.
The main Table is speech which contained the English text of the speech as well as an speechId.
With this ID the speeches can be connected to the tables speechMetaData and spokenAs.
The table speechMetaData contains the information of the Informations AgendaItem, Video, Date and Language as well as an ID that links to the Speaker table.
The spokenAs table connects the Speech to a function table which contains the function as well as the Institution of the speaker.
 To make queries on the whole data, I loaded the data into a pandas object using the following code.

<h2> Generating Sub corpora </h2>
<h3> in the following section it is explained how we got from the unconnected data to the sub corpora that were needed</h3>
{% highlight python %}
import pandas

speeches = pandas.read_csv("data/processed/octavo/English-speeches.csv", escapechar='\\', index_col='speech' , error_bad_lines=False, quotechar = '"')
speechMetadata = pandas.read_csv("data/processed/octavo/speechMetadata.csv", escapechar='\\', index_col='speech', error_bad_lines=False, quotechar = '"')
institution = pandas.read_csv("data/processed/octavo/institutions.csv", escapechar='\\', index_col='institution', error_bad_lines=False, quotechar = '"')
politicalFunctions = pandas.read_csv("data/processed/octavo/politicalFunctions.csv", escapechar='\\', index_col='politicalFunction', error_bad_lines=False, quotechar = '"')
agendaItems = pandas.read_csv("data/processed/octavo/agendaItems.csv", escapechar='\\', index_col='agendaItem', error_bad_lines=False, quotechar = '"')
spokenAs = pandas.read_csv("data/processed/octavo/spokenAs.csv", escapechar='\\', index_col='speech', error_bad_lines=False, quotechar = '"')
speakers = pandas.read_csv("data/processed/octavo/speakers.csv", escapechar='\\', index_col='person', error_bad_lines=False, quotechar = '"')
{% endhighlight %}

Then i started building the sub corpora, which i will explain on the example of the future.
To do so i first created an array containing the keywords we selected.

{% highlight python %}
keywords = ['face the future','determine future','affect future',
'define future','jeopardize future','sustainable future','near future',
'not-to-distant future','distant future','future goal','future dynamics',
'prosperous future','Future of Europe','Future of European Union','Future of our model',
'Hope for the future','Shape the future','Future prospects','Secure the future',
'Political future','economical future','Towards to the future','Challenges of the future',
'Future generations','Future threat','Future strategy','Future catastrophe','future development',
'build future','our future','common future']
{% endhighlight %}

Now i build a function to select the speeches and sentences from the Dataset
which contained the keywords. Therefor i created three lists, two to hold the
newly generated subset of speeches and sentences called sub corpora and
sentences.  And one with punctuation characters to subdivide speeches into sentences.

{% highlight python %}
subcorpora = []
sentences = []
punctuation=['?', ';', '.', '!']
{% endhighlight %}

In the following code i ran through all speeches. Saving the text to the variable i and the index to j.
Afterwards I checked for every speech whether the text contains any keyword from the keyword list. 
When there is a match for a keyword in a speech, and there has not been a match for this speech so far i add the speech to the sub corpora. Then i subdivide the speech into sentences and select the one which matches and save it to the sentences list, with the keyword that matched.
{% highlight python %}
for j,i in enumerate(data['text']):
        if j%10000==0:
            print(str(j/len(data)*100)+"%")
        text = True
        for keyword in keywords:
            if keyword.lower() in i.lower():
                if text:
                    subcorpora.append([a.index[j],keyword,i])
                    text = False
                spliti = [i]
                for x in punctuation:
                    tmp = []
                    for y in spliti:
                        tmp = tmp + y.split(x)
                    spliti = tmp
                for sentence in spliti:
                    if keyword.lower() in sentence.lower():
                        sentences.append([a.index[j],keyword,sentence])     
{% endhighlight %}

After generating those lists i created new data frames for the sub corpora and the sentences.
{% highlight python %}
subdf = pandas.DataFrame(subcorpora, columns=['speech','keyword','text'])
sendf = pandas.DataFrame(sentances, columns=['speech','keyword','sentence'])
{% endhighlight %}

In the last step i save those corpora to csv files so my team members could work with them.
{% highlight python %}
subdf.to_csv("data/processed/octavo/Future/Future-speeches-keywords.csv",index=False)
sendf.to_csv("data/processed/octavo/Future/Future-sentances-keywords.csv",index=False)
{% endhighlight %}

And then i saved them again without the keywords and for the sentences i removed double sentences which matched multiple keywords.
{% highlight python %}
del subdf['keyword']
del sendf['keyword']
subdf.to_csv("data/processed/octavo/Future/Future-speeches.csv",index=False)
sendf = sendf.groupby(sendf['sentence']).first()
sendf['sentence'] = sendf.index
sendf.to_csv("data/processed/octavo/Future/Future-sentances.csv",index=False)
{% endhighlight %}

<h2> Adding more informations to the datasets</h2>
<h3> In the following i will explain how i added the information my group members needed</h3>

There for i loaded the sub corpora they needed as well as the needed other  files as they can be seen in the graph above.
Then then i joined those files by their and the foreign id's.
{% highlight python %}
import pandas
speeches = pandas.read_csv("data/processed/octavo/Totalitarian/Totalitarian-sentances.csv", escapechar='\\', index_col='speech' , error_bad_lines=False, quotechar = '"')
speechMetadata = pandas.read_csv("data/processed/octavo/speechMetadata.csv", escapechar='\\', index_col='speech', error_bad_lines=False, quotechar = '"')
speakers = pandas.read_csv("data/processed/octavo/speakers.csv", escapechar='\\', index_col='person', error_bad_lines=False, quotechar = '"')
speech_Meta = speeches.join(speechMetadata)
speech_Meta_Speaker = speech_Meta.join(speakers, on='speaker')
{% endhighlight %}

After generating the big dataset i selected the required subset and saved it into a new file.

{% highlight python %}
data = speech_Meta_Speaker[['sentence','country','date','gender']]
data.to_csv("data/processed/octavo/Totalitarian/Totalitarian-sentence-date-country.csv")
{% endhighlight %}


<h2> Removing none english speeches </h2>
<h3> In the following i will explain how i removed the speeches that were not in English </h3>

To do so i used the python/pip package langdetect as well as pandas.
First i again loaded the speeches into a pandas data frame.
{% highlight python %}
import pandas
from langdetect import detect
speeches = pandas.read_csv("data/processed/octavo/English-speeches.csv", escapechar='\\', index_col='speech' , error_bad_lines=False, quotechar = '"')
{% endhighlight %}
Then i created a list called list to save the indexes of the speeches that were not in English.
{% highlight python %}
lst = []
{% endhighlight %}
To select the none English speeches, this algorithm goes through every speech and checks if the language of the speech is not English and then adds it to the list.
Since i had a problem with a lot of short speeches with many none English names
where wrongly categorized as none English i added the feature to only look
at speeches that are longer than 100 characters.  This is in our case valid
since the shorter speeches are not likely about a topic but just some organizational stuff.
This hypothesis was supported by close reading. 
{% highlight python %}
for j,i in enumerate(speeches['text']):
    if j%1000==0:
        print(str(j/len(speeches)*100)+"%")
    if len(i) > 100:
        lang = detect(i)
        if lang != 'en':
            print(lang,len(i),i)
            lst.append(j)
{% endhighlight %}
After building the list i then removed all speeches with the indexes in the list from the corpora.
And then i saved it as a new csv file to be used for generating all the required sub corpora.
{% highlight python %}
speeches = speeches.reset_index()
a = speeches
a = a.drop(a.index[lst])
a.to_csv("data/processed/octavo/Only-English-speeches.csv",index=False)
{% endhighlight %}
