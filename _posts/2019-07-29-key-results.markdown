---
layout: post
title:  "Link collection to key results"
date:   2019-07-30 00:00:01 -0300
categories: liks update
image: parliament.jpg
---
In the following there are the Links to our key results:

We did two presentations the [Progress update][update] and the [Final Presentation][pres]

And we created a [Poster][poster].

[update]: https://docs.google.com/presentation/d/1o9aHmNNlDz64vd8ZMiynsLO9JLXWOskfwhte8LEfKo8/edit#slide=id.p
[pres]:   https://docs.google.com/presentation/d/16wR4yKFpqXpnByVshc3gyt21k7qIbDpOcuoFa9dtMBk/edit#slide=id.p
[poster]: https://drive.google.com/open?id=1xjSqx3q4Z5rXB8U41PapyjxpOJn4-wdz
