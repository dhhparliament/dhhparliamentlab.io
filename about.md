---
layout: page
title: About
permalink: /about/
---

On this website I describe the Group Project of the Parliament Group on the Helsinki DHH19 Hackathon.
With a focus on my work in cleaning and preparing the Data.

[Twitter](https://twitter.com/dhh19_EP)
[Medium](https://medium.com/@dhh19parliament)

